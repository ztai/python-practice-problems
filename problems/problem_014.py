# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    pasta_time = ["eggs","flour", "oil"]
    ingredients = sorted(ingredients)
    if ingredients == pasta_time:
        return True
    return False

    has_counter = 0
    has_oil = False
    has_flour = False
    has_eggs = False

    for ing in ingredients:
        if ing == "eggs":
            has_eggs = True
        if ing == "flour":
            has_flour = True
        if ing == "oil":
            has_oil = True

    pass

print(can_make_pasta(["flour","eggs","oil"]))
