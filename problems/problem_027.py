# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    return max(values)

    max_val = 0
    for val in values:
        if val > max_val:
            max_val = val
    return val
    pass


print(max_in_list([1,2,3]))
print(max_in_list([3295235,2395,239523,95239,52,9,9]))
