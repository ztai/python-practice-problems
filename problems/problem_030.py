# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    biggest_value = 0
    second_biggest_value = 0
    for value in values:
        if value > biggest_value:
            second_biggest_value = biggest_value
            biggest_value = value
        elif value > second_biggest_value:
            second_biggest_value = value
    return second_biggest_value
    pass

print(find_second_largest([2,3,4,5,6]))
print(find_second_largest([234,235233,5235,23,5623,6,236]))
print(find_second_largest([234,23,56326235,23,5623,6,236]))
print(find_second_largest([234,23,5232125,23,5623,62424,1241236]))
